<?php

/*
Plugin Name: Ripple Toolkit
Plugin URI:  http://karlos.com.au
Description: Compainion Plugin for all ripple themes
Version:     1
Author:      Karlos Sarunic
Author URI:  http://karlos.com.au
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

require( 'option-page.php' );
require( 'shortcodes.php' );
require( 'widgets/ripple_recent_posts.php' );
require( 'custom-posts.php' );

require( 'acf_fields/acf_field_groups.php' );
require( 'mega-menu.php' );

define( 'PLUGIN_VERSION', '1');


add_action( 'widgets_init', 'ks_widget_setup' );
function ks_widget_setup(){
    register_widget('Ripple_Widget_Recent_Posts');
}