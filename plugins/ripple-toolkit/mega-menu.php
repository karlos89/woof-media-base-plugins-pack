<?php
function ks_get_mega_menu(){
	$args = array( 'post_type' => 'mega-menu' );
	$mega_loop = new WP_Query( $args );
	$mega_classes = array();
	
	if( $mega_loop->have_posts() ):
		ob_start();	
		echo '<div class="mega-menu-container">';
		
		while( $mega_loop->have_posts() ): $mega_loop->the_post();
			$mega_class = get_field( 'menu_item_id' );
			$mega_classes[] = $mega_class;
			
			echo sprintf( '<div id="%s" class="mega-menu row mega-%s"><div class="wrap">', $mega_class,$mega_class );
				if( have_rows( 'mega_menu_items' ) ):
					while( have_rows( 'mega_menu_items' ) ): the_row();
				        
				        switch( get_row_layout() ):
				        	case 'page_links':
				        		ks_mega_menu_get_menu();
								break;
				        	case 'feature_tile':
				        		ks_mega_menu_get_feature_tile();
								break;
				        	case 'search':
				        		ks_mega_menu_get_search();
								break;
				        	case 'text':
				        		ks_mega_menu_get_text();
								break;
				        	default:
				        		ks_mega_menu_get_menu();		
				        endswitch;
				        
					endwhile;
				endif;
				
			echo '</div></div>';
		endwhile;
		
		echo '</div>';
		$output = ob_get_clean();
	endif;
	wp_reset_postdata();
	
	return $output;
}

function ks_mega_menu_get_menu(){
	
	echo '<div class="widget widget_nav_menu"><div class="widget-wrap">';
	echo sprintf( '<h4>%s</h4>',get_sub_field( 'menu_title' ) );
	
	if( have_rows( 'page_link' ) ):
		echo '<ul class="menu">';
		while( have_rows( 'page_link' ) ): the_row();
			echo sprintf( '<li class="menu-item"><a href="%s">%s</a></li>', get_sub_field( 'url' ), get_sub_field( 'link_name' ) );
		endwhile;
		echo '</ul>';
	endif;

	echo '</div></div>';
	
}

function ks_mega_menu_get_feature_tile(){
	
	$menu_image = get_sub_field( 'image' );
	$menu_link = "#";
	
	if( have_rows( 'page_link' ) ):
		while( have_rows( 'page_link' ) ): the_row();
			$menu_link = get_sub_field( 'url' );
		endwhile;
	endif;

	echo '<div class="widget widget_nav_menu"><div class="widget-wrap">';
	echo sprintf( '<a class="mega-img-tile" href="%s"><img src="%s" alt="%s"/></a>', $menu_link, $menu_image['url'], $menu_image['alt'] );
	echo '</div></div>';
	
}

function ks_mega_menu_get_search(){
	
	$menu_image = get_sub_field( 'image' );
	$menu_link = "#";
	
	if( have_rows( 'page_link' ) ):
		while( have_rows( 'page_link' ) ): the_row();
			$menu_link = get_sub_field( 'url' );
		endwhile;
	endif;

	echo '<div class="widget widget-double widget_nav_menu"><div class="widget-wrap search-widget">';
	get_search_form();
	echo '</div></div>';
	
}

function ks_mega_menu_get_text(){
	
	$menu_image = get_sub_field( 'image' );
	$menu_link = "#";
	
	if( have_rows( 'page_link' ) ):
		while( have_rows( 'page_link' ) ): the_row();
			$menu_link = get_sub_field( 'url' );
		endwhile;
	endif;

	echo '<div class="widget widget-double widget_nav_menu"><div class="widget-wrap">';
	echo get_sub_field( 'text' );
	echo '</div></div>';
	
}