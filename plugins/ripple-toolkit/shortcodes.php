<?php

/**
 * Shortcode that creates a clickable button.
 * text="click me" text displayed on the button
 * align="left/center/right" button possition
 * url="link location" full url path
 * tab="true/false" true opens in new tab false opens in same tab
 * category "cats"  used for google analytics
 * label "kittens"  used for google analytics
 * 
 * @param  [type] $atts
 * @return [type]
 */
function ks_shortcode_button($atts){
	// Defaults
	extract(shortcode_atts(array(
	  "text" => 'Read More',
	  "align" => 'left',
	  "url" => '#',
	  "tab" =>	false,
	  "category" => '',
	  "label" => '',
	  "action" => 'click'
	), $atts));
	
	if( !empty( $label ) ):
		$tracking = "onClick=\"ga('send', 'event', { eventCategory: '$category', eventAction: '$action', eventLabel: '$label'});\"";
	endif;

	if( $tab ):
		$taget = "target=\"_blank\"";
	endif;

	switch($align){
		case 'left':
		case 'right':
		case 'center':
		break;
		default:
			$align = 'left';
	}
	
	$output = '<div class="" style="text-align:'.$align.'">
				<a class="button" href="'.$url.'" '.$tracking.' '.$taget.'>'.$text.'</a>
				</div>';
	
	return $output;
}
add_shortcode('button', 'ks_shortcode_button' );

function ks_shortcode_testimonial($atts){

	ob_start();
	// Defaults
	extract(shortcode_atts(array(
	  "id" => '',
	  "short" => 'no'
	), $atts));

	/// query is made   
	if( !empty( $id ) ){
		$args = "showposts=1&post_type=testimonials&posts_per_page=1&p=$id";
	}else{         
		$args = 'showposts=1&post_type=testimonials&posts_per_page=1&orderby=rand';
	}
	$the_query = new WP_Query( $args );

	// the loop
	if ( $the_query->have_posts()) : while ( $the_query->have_posts()) : $the_query->the_post(); ?>
		<div class="shortcode-testimonial">
			<div class="testimonial-simple">
				<div class="testimonial">
					<?php
					if( strtolower($short) == 'yes' ):
						if( get_field( 'test_short_testimonial' ) ):
							the_field( 'test_short_testimonial' ); 
						endif;
					else:
						if( get_field( 'test_long_testimonial' ) ):
							the_field( 'test_long_testimonial' ); 
						endif;
					endif;
					?>
				</div>
				<span class="testimonial-author">
					<?php 
					if( get_field( 'test_full_name' ) ):
						the_field( 'test_full_name' ); 
					endif;

					if( get_field( 'test_company' ) ):
						echo ", ";
						the_field( 'test_company' ); 
					endif;

					if( get_field( 'test_location' ) ):
						echo ", ";
						the_field( 'test_location' ); 
					endif;
					?>
				</span>
			</div>
		</div>
		<?php 
		wp_reset_postdata();
		endwhile;
	endif;

	$function_data = ob_get_clean();
	return $function_data;
}
add_shortcode('testimonial', 'ks_shortcode_testimonial' );

/*
 * Shortcode to generate a list of FAQ's in accordian form.
 * using id for a specific FAQ or topic for a category of FAQs
 */
function ks_shortcode_faq($atts){

	ob_start();
	// Defaults
	extract(shortcode_atts(array(
	  "id" => '',
	  "group" => ''
	), $atts));

	echo $id;
	/// query is made   
	if( !empty( $id ) ){
		$args = array(
				'showposts' =>1,
				'post_type' => 'faq',
				'posts_per_page' =>1,
				'p' =>$id
				);
	}else{           
		$args = array(
				'post_type' => 'faq',
				'posts_per_page' =>0,
				'faq-groups' => $group
				);
	}
	$the_query = new WP_Query( $args );
	// the loop
	if ( $the_query->have_posts()) : ?>
		<div class="accordion shortcode-faq">
			<?php while ( $the_query->have_posts()) : $the_query->the_post(); ?>
				<h2 class="faq-question accordion-toggle"><?php the_title(); ?></h2>
				<div class="faq-answer accordion-content"><?php the_content();?></div>
			<?php endwhile;?>
		</div>
		<?php wp_reset_postdata();
	endif;

	$function_data = ob_get_clean();
	return $function_data;
}
add_shortcode('faq', 'ks_shortcode_faq' );