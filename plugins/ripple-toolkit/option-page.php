<?php

add_action( 'admin_menu', 'ripple_cpt_menu' );

function ripple_cpt_menu() {
	add_options_page( 'Ripple CPT Options', 'Ripple CPT Options', 'manage_options', 'ripple-cpt-options', 'ripple_cpt_options' );
}

/** Step 3. */
function ripple_cpt_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}?>

<div class="wrap">
	<h2>Ripple Custom Post Types</h2>
	<p>Nothing To see here yet.</p>
</div>

	<?php
}