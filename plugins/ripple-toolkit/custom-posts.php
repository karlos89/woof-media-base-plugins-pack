<?php 
/**
 *  Handles creation and inclusion of all custom post types
 *  
 */

/** Standard Includes **/
add_action( 'init', 'ks_custom_posts_init_testimonials' );
add_action( 'init', 'ks_custom_posts_init_faq' );
add_action('init', 'ks_custom_posts_init_cta');

add_action('init', 'ks_custom_posts_init_megamenu');
add_action('init', 'ks_custom_posts_init_services');
//add_action( 'init', 'ks_custom_posts_init_quotes' );
//add_action( 'init', 'ks_custom_posts_init_people' );
//add_action('init', 'ks_custom_posts_init_session');

add_action('admin_enqueue_scripts', 'my_admin_theme_style');
function my_admin_theme_style() {
    wp_enqueue_style('my-admin-style', plugins_url( '/admin.css', __FILE__ ), array(), PLUGIN_VERSION);
}
add_action( 'wp_enqueue_scripts', 'ripple_cpt_script_includes' );
function ripple_cpt_script_includes() {
	wp_enqueue_script( 'ripple-cpt-js', plugins_url( '/js/ripple-js.js', __FILE__ ), array('jquery'), PLUGIN_VERSION );

	wp_enqueue_style( 'ripple-cpt-style', plugins_url( 'style.css', __FILE__ ), array(), PLUGIN_VERSION );
}

/**
*   Creates Testimonials Custom post type with taxonomy
*/
function ks_custom_posts_init_testimonials() {
    $labels = array(
        'name'              => 'Testimonials',
        'singular_name'     => 'Testimonial',
        'add_new'           => 'Add New',
        'add_new_item'      => 'Add New Testimonial',
        'edit_item'         => 'Edit Testimonial',
        'new_item'          => 'New Testimonial',
        'all_items'         => 'All Testimonials',
        'view_item'         => 'View Testimonial',
        'search_items'      => 'Search Testimonials',
        'not_found'         => 'No Testimonials found',
        'not_found_in_trash'=> 'No Testimonials found in Trash',
        'parent_item_colon' => '',
        'menu_name'         => 'Testimonials'
    );
 
    $args = array(
        'labels'            => $labels,
        'public'            => true,
        'menu_icon'         => '',
        'publicly_queryable'=> true,
        'show_ui'           => true,
        'show_in_menu'      => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'testimonials' ),
        'capability_type'   => 'post',
        'has_archive'       => false,
        'hierarchical'      => false,
        'menu_position'     => null,
        'supports'          => array( 'title', 'editor', 'author', 'thumbnail' )
    );
 
register_post_type( 'testimonials', $args );

	$labels = array(
        'name'              => __( 'Groups' ),
        'singular_name'     => __( 'Group' ),
        'search_items'      => __( 'Search Groups' ),
        'all_items'         => __( 'All Groups' ),
        'parent_item'       => __( 'Parent Group' ),
        'parent_item_colon' => __( 'Parent Group:' ),
        'edit_item'         => __( 'Edit Group' ), 
        'update_item'       => __( 'Update Group' ),
        'add_new_item'      => __( 'Add New Group' ),
        'new_item_name'     => __( 'New Group' ),
        'menu_name'         => __( 'Groups' ),
    ); 
    $args = array(
        'labels'            => $labels,
        'public'            =>  true,
        'show_in_nav_menus' =>  false,
        'has_archive'       =>  false,
        'hierarchical'      =>  true,
        'rewrite'           =>  array('slug' => '/testimonial-group', 'with_front' => true),
    );
    register_taxonomy( 'testimonial-groups', 'testimonials', $args );
}

/*****************************
*   Adds extra column in admin panel for shortcode
******************************/
add_filter( 'manage_edit-testimonials_columns', 'ripple_testimonials_columns' ) ;

function ripple_testimonials_columns( $columns ) {

    $columns['shortcode'] = __( 'Shortcode' );

    return $columns;
}

add_action( 'manage_testimonials_posts_custom_column', 'ripple_manage_testimonials_columns', 10, 2 );

function ripple_manage_testimonials_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        /* If displaying the 'duration' column. */
        case 'shortcode' :
                echo '[testimonial id="'.$post_id.'"]';
                echo '[testimonial id="'.$post_id.'" short="yes"]';
            break;
        default :
            break;
    }
}

/**
*   Creates quotes Custom post type with taxonomy
*/
function ks_custom_posts_init_quotes() {
    $labels = array(
        'name'              => 'Quotes',
        'singular_name'     => 'Quote',
        'add_new'           => 'Add New',
        'add_new_item'      => 'Add New Quote',
        'edit_item'         => 'Edit Quote',
        'new_item'          => 'New Quote',
        'all_items'         => 'All Quotes',
        'view_item'         => 'View Quote',
        'search_items'      => 'Search Quotes',
        'not_found'         => 'No Quotes found',
        'not_found_in_trash'=> 'No Quotes found in Trash',
        'parent_item_colon' => '',
        'menu_name'         => 'Quotes'
    );
 
    $args = array(
        'labels'            => $labels,
        'public'            => true,
        'menu_icon'         => '',
        'publicly_queryable'=> true,
        'show_ui'           => true,
        'show_in_menu'      => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'quotes' ),
        'capability_type'   => 'post',
        'has_archive'       => false,
        'hierarchical'      => false,
        'menu_position'     => null,
        'supports'          => array( 'title', 'editor', 'author', 'thumbnail' )
    );
 
register_post_type( 'quotes', $args );

    $labels = array(
        'name'              => __( 'Groups' ),
        'singular_name'     => __( 'Group' ),
        'search_items'      => __( 'Search Groups' ),
        'all_items'         => __( 'All Groups' ),
        'parent_item'       => __( 'Parent Group' ),
        'parent_item_colon' => __( 'Parent Group:' ),
        'edit_item'         => __( 'Edit Group' ), 
        'update_item'       => __( 'Update Group' ),
        'add_new_item'      => __( 'Add New Group' ),
        'new_item_name'     => __( 'New Group' ),
        'menu_name'         => __( 'Groups' ),
    ); 
    $args = array(
        'labels'            => $labels,
        'public'            =>  true,
        'show_in_nav_menus' =>  false,
        'has_archive'       =>  false,
        'hierarchical'      =>  true,
        'rewrite'           =>  array('slug' => '/quote-groups', 'with_front' => true),
    );
    register_taxonomy( 'quote-groups', 'quotes', $args );
}

/*****************************
*   Adds extra column in admin panel for shortcode
******************************/
add_filter( 'manage_edit-quotes_columns', 'ripple_quotes_columns' ) ;

function ripple_quotes_columns( $columns ) {

    $columns['shortcode'] = __( 'Shortcode' );

    return $columns;
}

add_action( 'manage_quotes_posts_custom_column', 'ripple_manage_quotes_columns', 10, 2 );

function ripple_manage_quotes_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        /* If displaying the 'duration' column. */
        case 'shortcode' :
                echo '[quote id="'.$post_id.'"]';
            break;
        default :
            break;
    }
}

function ks_custom_posts_init_people() {
    $labels = array(
        'name'              => 'People',
        'singular_name'     => 'Person',
        'add_new'           => 'Add New',
        'add_new_item'      => 'Add New Person',
        'edit_item'         => 'Edit Person',
        'new_item'          => 'New Person',
        'all_items'         => 'All People',
        'view_item'         => 'View Person',
        'search_items'      => 'Search People',
        'not_found'         => 'No People found',
        'not_found_in_trash'=> 'No People found in Trash',
        'parent_item_colon' => '',
        'menu_name'         => 'People'
    );
 
    $args = array(
        'labels'            => $labels,
        'public'            => true,
        'menu_icon'         => '',
        'publicly_queryable'=> true,
        'show_ui'           => true,
        'show_in_menu'      => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'people' ),
        'capability_type'   => 'post',
        'has_archive'       => true,
        'hierarchical'      => false,
        'menu_position'     => null,
        'supports'          => array( 'title', 'editor', 'author', 'thumbnail' )
    );
 
register_post_type( 'people', $args );

    $labels = array(
        'name'              => __( 'Groups' ),
        'singular_name'     => __( 'Group' ),
        'search_items'      => __( 'Search Groups' ),
        'all_items'         => __( 'All Groups' ),
        'parent_item'       => __( 'Parent Group' ),
        'parent_item_colon' => __( 'Parent Group:' ),
        'edit_item'         => __( 'Edit Group' ), 
        'update_item'       => __( 'Update Group' ),
        'add_new_item'      => __( 'Add New Group' ),
        'new_item_name'     => __( 'New Group' ),
        'menu_name'         => __( 'Groups' ),
    ); 
    $args = array(
        'labels'            => $labels,
        'public'            =>  true,
        'show_in_nav_menus' =>  false,
        'has_archive'       =>  false,
        'hierarchical'      =>  true,
        'rewrite'           =>  array('slug' => '/groups', 'with_front' => true),
    );
    register_taxonomy( 'people-groups', 'people', $args );
}

function ks_custom_posts_init_faq() {
    $labels = array(
        'name'              => 'FAQ',
        'singular_name'     => 'FAQ',
        'add_new'           => 'Add New',
        'add_new_item'      => 'Add New FAQ',
        'edit_item'         => 'Edit FAQ',
        'new_item'          => 'New FAQ',
        'all_items'         => 'All FAQ',
        'view_item'         => 'View FAQ',
        'search_items'      => 'Search FAQ',
        'not_found'         => 'No FAQ found',
        'not_found_in_trash'=> 'No FAQ found in Trash',
        'parent_item_colon' => '',
        'menu_name'         => 'FAQ'
    );
 
    $args = array(
        'labels'            => $labels,
        'public'            => true,
        'menu_icon'         => '',
        'publicly_queryable'=> true,
        'show_ui'           => true,
        'show_in_menu'      => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'faq' ),
        'capability_type'   => 'post',
        'has_archive'       => false,
        'hierarchical'      => false,
        'menu_position'     => null,
        'supports'          => array( 'title', 'editor', 'author', 'thumbnail' )
    );
 
register_post_type( 'faq', $args );

    $labels = array(
        'name'              => __( 'Groups' ),
        'singular_name'     => __( 'Group' ),
        'search_items'      => __( 'Search Groups' ),
        'all_items'         => __( 'All Groups' ),
        'parent_item'       => __( 'Parent Group' ),
        'parent_item_colon' => __( 'Parent Group:' ),
        'edit_item'         => __( 'Edit Group' ), 
        'update_item'       => __( 'Update Group' ),
        'add_new_item'      => __( 'Add New Group' ),
        'new_item_name'     => __( 'New Group' ),
        'menu_name'         => __( 'Groups' ),
    ); 
    $args = array(
        'labels'            => $labels,
        'public'            =>  true,
        'show_in_nav_menus' =>  false,
        'has_archive'       =>  false,
        'hierarchical'      =>  true,
        'rewrite'           =>  array('slug' => '/faq-groups', 'with_front' => true),
    );
    register_taxonomy( 'faq-groups', 'faq', $args );
}

/*****************************
*   Adds extra column in admin panel for shortcode
******************************/
add_filter( 'manage_edit-faq_columns', 'ripple_faq_columns' ) ;

function ripple_faq_columns( $columns ) {

    $columns['shortcode'] = __( 'Shortcode' );

    return $columns;
}

add_action( 'manage_faq_posts_custom_column', 'ripple_manage_faq_columns', 10, 2 );

function ripple_manage_faq_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        /* If displaying the 'duration' column. */
        case 'shortcode' :
                echo '[faq id="'.$post_id.'"]';
                $categories = get_the_terms( $post_id, 'faq-groups' );
                if( $categories && is_array( $categories ) ):
                    foreach ($categories as $key => $category) {
                    	echo '<br/>[faq group="'.$category->name.'"]';
                    }
                endif;
            break;
        default :
            break;
    }
}

function ks_custom_posts_init_cta() {
    $labels = array(
        'name'              => 'Calls to action',
        'singular_name'     => 'Call to action',
        'add_new'           => 'Add New',
        'add_new_item'      => 'Add New Call to action',
        'edit_item'         => 'Edit Call to action',
        'new_item'          => 'New Call to action',
        'all_items'         => 'All Calls to action',
        'view_item'         => 'View Calls to action',
        'search_items'      => 'Search Calls to action',
        'not_found'         => 'No Calls to action found',
        'not_found_in_trash'=> 'No Calls to action found in Trash',
        'parent_item_colon' => '',
        'menu_name'         => 'Call to action'
    );
 
    $args = array(
        'labels'            => $labels,
        'public'            => true,
        'menu_icon'         => '',
        'publicly_queryable'=> true,
        'show_ui'           => true,
        'show_in_menu'      => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'cta' ),
        'capability_type'   => 'post',
        'has_archive'       => false,
        'hierarchical'      => false,
        'menu_position'     => null,
        'supports'          => array( 'title', 'editor', 'author', 'thumbnail' )
    );
 
register_post_type( 'cta', $args );

}

function ks_custom_posts_init_session() {
    $labels = array(
        'name'              => 'Sessions',
        'singular_name'     => 'Session',
        'add_new'           => 'Add New',
        'add_new_item'      => 'Add New Session',
        'edit_item'         => 'Edit Session',
        'new_item'          => 'New Session',
        'all_items'         => 'All Sessions',
        'view_item'         => 'View Sessions',
        'search_items'      => 'Search Sessions',
        'not_found'         => 'No Sessions found',
        'not_found_in_trash'=> 'No Sessions found in Trash',
        'parent_item_colon' => '',
        'menu_name'         => 'Sessions'
    );
 
    $args = array(
        'labels'            => $labels,
        'public'            => true,
        'menu_icon'         => '',
        'publicly_queryable'=> true,
        'show_ui'           => true,
        'show_in_menu'      => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'session' ),
        'capability_type'   => 'page',
        'has_archive'       => false,
        'hierarchical'      => false,
        'menu_position'     => 5,
        'supports'          => array( 'title', 'editor', 'author', 'thumbnail' )
    );
 
register_post_type( 'session', $args );

}

function ks_custom_posts_init_megamenu() {
    $labels = array(
        'name'              => 'Mega Menu',
        'singular_name'     => 'Mega Menu',
        'add_new'           => 'Add New',
        'add_new_item'      => 'Add New Menu Item',
        'edit_item'         => 'Edit Item',
        'new_item'          => 'New Item',
        'all_items'         => 'All Menu Items',
        'view_item'         => 'View Menu Items',
        'search_items'      => 'Search Menu Items',
        'not_found'         => 'No Menu Items found',
        'not_found_in_trash'=> 'No Menu Items found in Trash',
        'parent_item_colon' => '',
        'menu_name'         => 'Mega Menu'
    );
 
    $args = array(
        'labels'            => $labels,
        'public'            => true,
        'menu_icon'         => '',
        'publicly_queryable'=> true,
        'show_ui'           => true,
        'show_in_menu'      => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'cta' ),
        'capability_type'   => 'post',
        'has_archive'       => false,
        'hierarchical'      => false,
        'menu_position'     => null,
        'supports'          => array( 'title' )
    );
 
register_post_type( 'mega-menu', $args );

}

function ks_custom_posts_init_services() {
    $labels = array(
        'name'              => 'Services',
        'singular_name'     => 'Service',
        'add_new'           => 'Add New',
        'add_new_item'      => 'Add New Service',
        'edit_item'         => 'Edit Service',
        'new_item'          => 'New Service',
        'all_items'         => 'All Services',
        'view_item'         => 'View Service',
        'search_items'      => 'Search Services',
        'not_found'         => 'No Services found',
        'not_found_in_trash'=> 'No Services found in Trash',
        'parent_item_colon' => '',
        'menu_name'         => 'Services'
    );
 
    $args = array(
        'labels'            => $labels,
        'public'            => true,
        'menu_icon'         => '',
        'publicly_queryable'=> true,
        'show_ui'           => true,
        'show_in_menu'      => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'services' ),
        'capability_type'   => 'post',
        'has_archive'       => false,
        'hierarchical'      => false,
        'menu_position'     => null,
        'supports'          => array( 'title', 'editor', 'author', 'thumbnail' )
    );
 
register_post_type( 'services', $args );

    $labels = array(
        'name'              => __( 'Type' ),
        'singular_name'     => __( 'Type' ),
        'search_items'      => __( 'Search Types' ),
        'all_items'         => __( 'All Types' ),
        'parent_item'       => __( 'Parent Type' ),
        'parent_item_colon' => __( 'Parent Type:' ),
        'edit_item'         => __( 'Edit Type' ), 
        'update_item'       => __( 'Update Type' ),
        'add_new_item'      => __( 'Add New Type' ),
        'new_item_name'     => __( 'New Type' ),
        'menu_name'         => __( 'Types' ),
    ); 
    $args = array(
        'labels'            => $labels,
        'public'            =>  true,
        'show_in_nav_menus' =>  false,
        'has_archive'       =>  false,
        'hierarchical'      =>  true,
        'rewrite'           =>  array('slug' => '/services-type', 'with_front' => true),
    );
    register_taxonomy( 'services-type', 'services', $args );
}

/*****************************
*   Adds extra column in admin panel for shortcode
******************************/
add_filter( 'manage_edit-services_columns', 'ripple_services_columns' ) ;

function ripple_services_columns( $columns ) {

    $columns['shortcode'] = __( 'Shortcode' );

    return $columns;
}

add_action( 'manage_services_posts_custom_column', 'ripple_manage_services_columns', 10, 2 );

function ripple_manage_services_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        /* If displaying the 'duration' column. */
        case 'shortcode' :
                echo '[services id="'.$post_id.'"]';
                $categories = get_the_terms( $post_id, 'services-type' );
                if( $categories && is_array( $categories ) ):
                    foreach ($categories as $key => $category) {
                        echo '<br/>[services group="'.$category->slug.'"]';
                    }
                endif;
            break;
        default :
            break;
    }
}