jQuery(document).ready(function($) {
/*
	 * Accordion script for FAQ display.
	 */
	$('.accordion').find('.accordion-toggle').click(function(){
		
		//Expand or collapse this panel
		$(this).next().slideToggle('fast');
		$(this).toggleClass("focused");
      
		//Hide the other panels
		$(".accordion-content").not($(this).next()).slideUp('fast');
		$(".accordion-toggle").not($(this)).removeClass("focused");
    }); 
    
    
    
    //*******************************************************
	//Mega Menu scripts
	//*******************************************************
		if( true ){	
			
		//On touch event for mobile devices
		$( '.site-header .mega-nav, .mega-menu > a' ).on('touchstart', function(){
			$(".mega-menu").css({"opacity" : 0, "left" : "-9999px"});
			
			var mega_id = '.mega-'+$(this).attr('id');
			$( mega_id ).css({"opacity" : 1, "left" : "0"});
			 return false; // prevent default action and stop event propagation
		});
		
		$( document ).on( "touchstart", function() {
			setTimeout(function(){$(".mega-menu").css({"opacity" : 0, "left" : "-9999px"});},500)
		});
	
		
		//Hover event for deskop devices
		$( '.site-header .mega-nav, .mega-menu' ).hover( function(){
			var mega_id = '.mega-'+$(this).attr('id');
			$( mega_id ).css({"opacity" : 1, "left" : "0"});
		},function(){
			var mega_id = '.mega-'+$(this).attr('id');
			$( mega_id ).css({"opacity" : 0, "left" : "-9999px"});
		});
	}

});